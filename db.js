const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2017331", "root", "",{host:"127.0.0.1", dialect:"mysql",logging:false,freezeTableName: true,
operatorsAliases: false});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//importovanje modela
db.Predmet = sequelize.import(__dirname + '/Predmet.js');
db.Grupa = sequelize.import(__dirname + '/Grupa.js');
db.Aktivnost = sequelize.import(__dirname + '/Aktivnost.js');
db.Dan = sequelize.import(__dirname + '/Dan.js');
db.Tip = sequelize.import(__dirname + '/Tip.js');
db.Student = sequelize.import(__dirname + '/Student.js');
db.StudentGrupa = sequelize.import(__dirname + '/StudentGrupa.js');

//relacije među tabelama
db.Predmet.hasMany(db.Grupa, {as: "grupePredmeta"});
db.Predmet.hasMany(db.Aktivnost, {as: "predmet"});
db.Dan.hasMany(db.Aktivnost, {as: "dan"});
db.Tip.hasMany(db.Aktivnost, {as: "tip"});
db.Student.hasMany(db.StudentGrupa, {as: "student"});
db.Grupa.hasMany(db.StudentGrupa, {as: "grupa"});

module.exports = db;
