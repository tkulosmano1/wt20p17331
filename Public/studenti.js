let Studenti = (function() {
    function GrupaImpl() {
        var ajaxGrupa = new XMLHttpRequest();
        ajaxGrupa.open("GET", "http://localhost:3000/v2/grupe", true);
        ajaxGrupa.onreadystatechange = function() {
            if (ajaxGrupa.readyState == 4 && ajaxGrupa.status == 200){
                var grupa = JSON.parse(this.responseText);
                var select = document.getElementById("grupa");
                for(var i = 0; i < grupa.length; i++) {
                    var option = document.createElement("option");
                    var text = document.createTextNode(grupa[i].naziv);
                    option.appendChild(text);
                    select.appendChild(option);
                }
            }
        }
        ajaxGrupa.send();
    }

    function StudentiGrupaImpl(studentiGrupaJSON) {
        var ajaxProvjeraStudentiGrupa = new XMLHttpRequest();
        ajaxProvjeraStudentiGrupa.open('POST', 'http://localhost:3000/v2/studenti-grupa', true);
        ajaxProvjeraStudentiGrupa.setRequestHeader("Content-Type", "application/json");
        ajaxProvjeraStudentiGrupa.getResponseHeader("Content-Type", "application/json");
        ajaxProvjeraStudentiGrupa.onreadystatechange = function() {
            if (ajaxProvjeraStudentiGrupa.readyState == 4 && ajaxProvjeraStudentiGrupa.status == 200) {
                var studenti = JSON.parse(this.responseText).message;
                alert(studenti);           
            }
        }
        ajaxProvjeraStudentiGrupa.send(JSON.stringify(studentiGrupaJSON));
    }

    function KlikImpl() {
        var grupa = document.getElementById("grupa").value;
        var studenti = document.getElementById("studenti").value;
        studenti = [...studenti.toString().split('\n')];
        var studentiGrupaJSON = [];

        for(var i = 0; i < studenti.length; i++) {
            var student = studenti[i].split(',');
            studentiGrupaJSON.push({ime: student[0], index: student[1], grupa: grupa});
        }
        Studenti.StudentiGrupa(studentiGrupaJSON);
    }

    return {
        Grupa: GrupaImpl,
        StudentiGrupa: StudentiGrupaImpl,
        Klik: KlikImpl
    }
}());