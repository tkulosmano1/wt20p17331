window.onload = function() {

    let okvir=document.getElementById("okvir");
    Raspored.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
    Raspored.dodajAktivnost(okvir,"WT","predavanje",19,21,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir,"WT","vježbe",12,13.5,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir,"RMA","predavanje",14,17,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir,"OR","predavanje",8,11,"Četvrtak");
    Raspored.dodajAktivnost(okvir,"OI","predavanje",12,15,"Srijeda");
    Raspored.dodajAktivnost(okvir,"OR","predavanje",8,22,"Četvrtak");
    Raspored.dodajAktivnost(okvir,"OR","vježbe",8,9.3,"Petak");
    Raspored.dodajAktivnost(okvir,"OR","vježbe",8.5,10.5,"Petak");
    Raspored.dodajAktivnost(okvir,"DM","tutorijal",15,17,"Utorak");
    Raspored.dodajAktivnost(okvir,"ASP","tutorijal",16,18,"Utorak");


    let okvir2=document.getElementById("okvir2");
    Raspored.iscrtajRaspored(okvir2,["Ponedjeljak","Utorak","Srijeda","Petak"],0,21);
    Raspored.dodajAktivnost(okvir2,"WT","predavanje",9,12,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir2,"WT","vježbe",12,13.5,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir2,"RMA","predavanje",14,17,"Ponedjeljak");
    Raspored.dodajAktivnost(okvir2,"RMA","vježbe",12.5,14,"Utorak");
    Raspored.dodajAktivnost(okvir2,"DM","tutorijal",13.5,16,"Utorak");
    Raspored.dodajAktivnost(okvir2,"DM","predavanje",16,19,"Utorak");
    Raspored.dodajAktivnost(okvir2,"OI","predavanje",12,15,"Srijeda");
    Raspored.dodajAktivnost(okvir2,"OR","predavanje",8,11,"Četvrtak");
    Raspored.dodajAktivnost(okvir2,"OR","vježbe",8,9.5,"Petak");

    let okvir3=document.getElementById("okvir3");
    Raspored.iscrtajRaspored(okvir3,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],-8,21);

    let okvir4=document.getElementById("okvir4");
    Raspored.iscrtajRaspored(okvir4,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],15,14);

    let okvir5=document.getElementById("okvir5");
    Raspored.iscrtajRaspored(okvir5,["Ponedjeljak","Petak"],8,11);

    let okvir6=document.getElementById("okvir6");
    Raspored.iscrtajRaspored(okvir6,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21.3);

    let okvir7=document.getElementById("okvir7");
    Raspored.iscrtajRaspored(okvir7,["Ponedjeljak","Utorak","Srijeda","Četvrtak"],8,21);
}
    
