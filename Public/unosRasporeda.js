let UnosRasporeda = (function() {
    function DanImpl() {
        var ajaxDan = new XMLHttpRequest();
        ajaxDan.open("GET", "http://localhost:3000/v2/dani", true);
        ajaxDan.onreadystatechange = function() {
            if (ajaxDan.readyState == 4 && ajaxDan.status == 200){
                var dan = JSON.parse(this.responseText);
                var select = document.getElementById("dan");
                for(var i = 0; i < dan.length; i++) {
                    var option = document.createElement("option");
                    var text = document.createTextNode(dan[i].naziv);
                    option.appendChild(text);
                    select.appendChild(option);
                }
            }
        }
        ajaxDan.send();
    }
    function TipImpl() {
        var ajaxTip = new XMLHttpRequest();
        ajaxTip.open("GET", "http://localhost:3000/v2/tipovi", true);
        ajaxTip.onreadystatechange = function() {
            if (ajaxTip.readyState == 4 && ajaxTip.status == 200){
                var tip = JSON.parse(this.responseText);
                var select = document.getElementById("tip");
                for(var i = 0; i < tip.length; i++) {
                    var option = document.createElement("option");
                    var text = document.createTextNode(tip[i].naziv);
                    option.appendChild(text);
                    select.appendChild(option);
                }
            }
        }
        ajaxTip.send();
    }
    function PredmetImpl() {
        var ajaxPredmet = new XMLHttpRequest();
        ajaxPredmet.open("GET", "http://localhost:3000/v2/predmeti", true);
        ajaxPredmet.onreadystatechange = function() {
            if (ajaxPredmet.readyState == 4 && ajaxPredmet.status == 200){
                var predmeti = JSON.parse(this.responseText);
                var div = document.getElementById("predmeti");
                var tabela = document.createElement("table");
                var tr = document.createElement("tr");
                var tdIndex = document.createElement("td");
                tdIndex.setAttribute("class","td-index");
                var tdPredmet = document.createElement("td");
                tdPredmet.setAttribute("class","td-celija");
                tdPredmet.innerText = "Predmet";
                tr.appendChild(tdIndex);
                tr.appendChild(tdPredmet);
                tabela.appendChild(tr);
                for(var i = 0; i < predmeti.length; i++) {
                    var tr1 = document.createElement("tr");
                    var tdIndex1 = document.createElement("td");
                    tdIndex1.setAttribute("class","td-index");
                    var tdPredmet1 = document.createElement("td");
                    tdPredmet1.setAttribute("class","td-celija");
                    tdIndex1.innerText = i+1;
                    tdPredmet1.innerText = predmeti[i].naziv;
                    tr1.appendChild(tdIndex1);
                    tr1.appendChild(tdPredmet1);
                    tabela.appendChild(tr1);
                }
                div.appendChild(tabela);
            }
        
            if (ajaxPredmet.readyState == 4 && ajaxPredmet.status == 404)
                document.getElementById("predmeti").innerHTML = "Greska: nepoznat URL";
        }
        ajaxPredmet.send();
    }

    function AktivnostImpl() {
        var ajaxAktivnost = new XMLHttpRequest();
        ajaxAktivnost.open("GET", "http://localhost:3000/v2/aktivnosti", true);
        ajaxAktivnost.onreadystatechange = function() {
            if (ajaxAktivnost.readyState == 4 && ajaxAktivnost.status == 200) {
                var aktivnosti = JSON.parse(this.responseText);
                var div = document.getElementById("aktivnosti");
                var tabela = document.createElement("table");
                var tr = document.createElement("tr");
                var tdIndex = document.createElement("td");
                tdIndex.setAttribute("class","td-index");
                var tdPredmet = document.createElement("td");
                tdPredmet.setAttribute("class","td-celija2");
                tdPredmet.innerText = "Predmet";
                var tdTip = document.createElement("td");
                tdTip.setAttribute("class","td-celija2");
                tdTip.innerText = "Tip";
                var tdPocetak = document.createElement("td");
                tdPocetak.setAttribute("class","td-celija2");
                tdPocetak.innerText = "Pocetak";
                var tdKraj = document.createElement("td");
                tdKraj.setAttribute("class","td-celija2");
                tdKraj.innerText = "Kraj";
                var tdDan = document.createElement("td");
                tdDan.setAttribute("class","td-celija2");
                tdDan.innerText = "Dan";
                tr.appendChild(tdIndex);
                tr.appendChild(tdPredmet);
                tr.appendChild(tdTip);
                tr.appendChild(tdPocetak);
                tr.appendChild(tdKraj);
                tr.appendChild(tdDan);
                tabela.appendChild(tr);
                for(var i = 0; i < aktivnosti.length; i++) {
                    var tr1 = document.createElement("tr");
                    var tdIndex1 = document.createElement("td");
                    tdIndex1.setAttribute("class","td-index");
                    tdIndex1.innerText = i+1;
                    var tdPredmet1 = document.createElement("td");
                    tdPredmet1.setAttribute("class","td-celija2");
                    tdPredmet1.innerText = aktivnosti[i].naziv;
                    var tdTip1 = document.createElement("td");
                    tdTip1.setAttribute("class","td-celija2");
                    tdTip1.innerText = aktivnosti[i].tip;
                    var tdPocetak1 = document.createElement("td");
                    tdPocetak1.setAttribute("class","td-celija2");
                    tdPocetak1.innerText = aktivnosti[i].pocetak;
                    var tdKraj1 = document.createElement("td");
                    tdKraj1.setAttribute("class","td-celija2");
                    tdKraj1.innerText = aktivnosti[i].kraj;
                    var tdDan1 = document.createElement("td");
                    tdDan1.setAttribute("class","td-celija2");
                    tdDan1.innerText = aktivnosti[i].dan;
                    tr1.appendChild(tdIndex1);
                    tr1.appendChild(tdPredmet1);
                    tr1.appendChild(tdTip1);
                    tr1.appendChild(tdPocetak1);
                    tr1.appendChild(tdKraj1);
                    tr1.appendChild(tdDan1);
                    tabela.appendChild(tr1);
                }
                div.appendChild(tabela);
            }       
            if (ajaxAktivnost.readyState == 4 && ajaxAktivnost.status == 404)
                document.getElementById("aktivnosti").innerHTML = "Greska: nepoznat URL";
        }
        ajaxAktivnost.send();
    }

    function BrisanjeImpl(nazivPredmeta) {
        var ajaxBrisanjePredmeta = new XMLHttpRequest();
        ajaxBrisanjePredmeta.open('DELETE', "http://localhost:3000/v2/predmet/" + nazivPredmeta, true);
        ajaxBrisanjePredmeta.onreadystatechange = function() {
            if (ajaxBrisanjePredmeta.readyState == 4 && ajaxBrisanjePredmeta.status == 200) {
                var porukaBrisanja = JSON.parse(this.responseText).message;
                alert(porukaBrisanja);
            }
        }
        ajaxBrisanjePredmeta.send();
    }

    function ProvjeraPredmetaImpl(nazivPredmeta, tipNastave , pocetakNastave, krajNastave, danNastave) {
        var predmet = {naziv: nazivPredmeta};
        var predmetJSON =  JSON.stringify(predmet);
    
        var ajaxProvjeraPredmeta = new XMLHttpRequest();
        ajaxProvjeraPredmeta.open('POST', 'http://localhost:3000/v2/predmet', true);
        ajaxProvjeraPredmeta.setRequestHeader("Content-Type", "application/json");
        ajaxProvjeraPredmeta.getResponseHeader("Content-Type", "application/json");
        ajaxProvjeraPredmeta.onreadystatechange = function() {
            if (ajaxProvjeraPredmeta.readyState == 4 && ajaxProvjeraPredmeta.status == 200) {
                var porukaPredmet = JSON.parse(this.responseText).message;
                UnosRasporeda.ProvjeraAktivnosti(nazivPredmeta, tipNastave , pocetakNastave, krajNastave, danNastave, porukaPredmet);
            }
        }
        ajaxProvjeraPredmeta.send(predmetJSON);
    }
    function ProvjeraAktivnostiImpl(nazivPredmeta, tipNastave , pocetakNastave, krajNastave, danNastave, porukaPredmet) {
        var aktivnost = {naziv: nazivPredmeta, tip: tipNastave, pocetak: parseFloat(pocetakNastave), kraj: parseFloat(krajNastave), dan: danNastave};
        var aktivnostJSON =  JSON.stringify(aktivnost);
                
        var ajaxProvjeraAktivnosti = new XMLHttpRequest();
        ajaxProvjeraAktivnosti.open('POST', 'http://localhost:3000/v2/aktivnost', true);
        ajaxProvjeraAktivnosti.setRequestHeader("Content-Type", "application/json");
        ajaxProvjeraAktivnosti.getResponseHeader("Content-Type", "application/json");
        ajaxProvjeraAktivnosti.onreadystatechange = function() {
            if (ajaxProvjeraAktivnosti.readyState == 4 && ajaxProvjeraAktivnosti.status == 200) {
                var porukaAktivnost = JSON.parse(this.responseText).message;
                if(porukaPredmet == "Uspješno dodan predmet!" && porukaAktivnost == "Aktivnost nije validna!") {
                    UnosRasporeda.Brisanje(nazivPredmeta);
                }
                else if(porukaPredmet == "Naziv predmeta postoji!" && porukaAktivnost == "Aktivnost nije validna!") {
                    alert("Aktivnost nije uspješno dodana!");
                }
                else {
                    alert("Uspješno dodana aktivnost!");
                    location.reload();
                }
            }
        }
        ajaxProvjeraAktivnosti.send(aktivnostJSON);
    }

    function KlikImpl() {
        var naziv = document.getElementById("naziv").value;
        var tip = document.getElementById("tip").value;
        var pocetakSatiMinute = [...document.getElementById("pocetak").value.split(":")];
        var pocetak = parseInt(pocetakSatiMinute[0]) + parseInt(pocetakSatiMinute[1])/60;
        var krajSatiMinute = [...document.getElementById("kraj").value.split(":")];
        var kraj = parseInt(krajSatiMinute[0]) + parseInt(krajSatiMinute[1])/60; 
        var dan = document.getElementById("dan").value;
        
        UnosRasporeda.ProvjeraPredmeta(naziv, tip , pocetak, kraj, dan);
    }

    return {
        Dan: DanImpl,
        Tip: TipImpl,
        Predmet: PredmetImpl,
        Aktivnost: AktivnostImpl,
        Brisanje: BrisanjeImpl,
        ProvjeraPredmeta: ProvjeraPredmetaImpl,
        ProvjeraAktivnosti: ProvjeraAktivnostiImpl,
        Klik: KlikImpl
    }
}());






