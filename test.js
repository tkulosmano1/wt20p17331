var assert = chai.assert;
describe('raspored', () => {
    describe('iscrtajRaspored()', () => {
        it('Iscrtavanje rasporeda1', () => {
            
            let okvir=document.getElementById("okvir");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var redovi = tabela.rows; 
            
            assert.equal(redovi.length, 6,"Greška");
            
        })
        it('Iscrtavanje rasporeda2', () => {
            
            let okvir=document.getElementById("okvir2");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var redovi = tabela.rows; 
            
            assert.equal(redovi.length, 5,"Greška");
            
        })
        it('Iscrtavanje rasporeda3', () => {
            
            let okvir=document.getElementById("okvir5");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var redovi = tabela.rows; 
            
            assert.equal(redovi.length, 3,"Greška");
            
        })
        it('Negativni sati', () => {
            
            let okvir=document.getElementById("okvir3");
            
            assert.equal(okvir.innerText, "Greška","Greška");
            
        })
        it('Sati nisu cijeli brojevi', () => {
            
            let okvir=document.getElementById("okvir6");
            
            assert.equal(okvir.innerText, "Greška","Greška");
            
        })
        it('Početak rasporeda veći od kraja rasporeda', () => {
            
            let okvir=document.getElementById("okvir3");
            
            assert.equal(okvir.innerText, "Greška","Greška");
            
        })
        it('Nepostojeći dan1', () => {
            
            let okvir=document.getElementById("okvir2");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var redovi = tabela.rows[4];
            var dan = redovi.cells[0];
            
            assert.notEqual(dan.innerText, "Četvrtak","Greška");
            
        })
        it('Nepostojeći dan2', () => {
            
            let okvir=document.getElementById("okvir5");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var redovi = tabela.rows[2];
            var dan = redovi.cells[0];
            
            assert.notEqual(dan.innerText, "Utorak","Greška");
            
        })
        it('Da li je vrijeme dobro1', () => {
            
            let okvir=document.getElementById("okvir5");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var red = tabela.rows[0];
            var brojac = 0;

            if(red.cells[1].innerText!="08:00") brojac++;
            if(red.cells[3].innerText!="10:00") brojac++;

            assert.equal(brojac, 0,"Greška");
        })
        it('Da li je vrijeme dobro2', () => {
            
            let okvir=document.getElementById("okvir");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var red = tabela.rows[0];
            var brojac = 0;

            if(red.cells[1].innerText!="08:00") brojac++;
            if(red.cells[3].innerText!="10:00") brojac++;
            if(red.cells[5].innerText!="12:00") brojac++;
            if(red.cells[8].innerText!="15:00") brojac++;
            if(red.cells[10].innerText!="17:00") brojac++;
            if(red.cells[12].innerText!="19:00") brojac++;

            assert.equal(brojac, 0,"Greška");
        })
    });
    describe('dodajAktivnost()', () => {
        it('Da li je početak nastave dobar', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.2,16,"Utorak");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var tr = tabela.rows[2];
            var brojac = 0;
            for(var i=2;i<tr.cells.length;i++){
                var td = tr.cells[i];
                if(td.style.backgroundColor == "lightblue"){
                    brojac++;
                }
            }
                
            assert.equal(brojac, 0,"Greška");
        }) 
        it('Da li je početak nastave dobar', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.5,16,"Ponedjeljak");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var tr = tabela.rows[1];
            var brojac = 0;
            for(var i=2;i<tr.cells.length;i++){
                var td = tr.cells[i];
                if(td.style.backgroundColor == "lightblue"){
                    brojac++;
                }
            }
                
            assert.equal(brojac, 5,"Greška");
        }) 
        it('Da li je kreirana tabela1', () =>{
            let okvir = document.getElementById("okvir3");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.5,16,"Ponedjeljak");
            var brojac = 0;

            if(okvir.getElementsByTagName("table").length != 0) brojac++;
                
            assert.equal(brojac, 0,"Greška");
        }) 
        it('Da li je kreirana tabela2', () =>{
            let okvir = document.getElementById("okvir");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.5,16,"Ponedjeljak");
            var brojac = 0;

            if(okvir.getElementsByTagName("table").length != 0) brojac++;
                
            assert.equal(brojac, 1,"Greška");
        }) 
        it('Da li je moguce dodati aktivnost u nepostojeci dan', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.5,16,"Petak");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            for(var i=1;i<tabela.rows.length;i++) {
                if(tabela.rows[i].cells[0].innerText == "Petak") brojac++;
            }
                
            assert.equal(brojac, 0,"Greška");
        }) 
        it('Da li je moguce dodati aktivnost u nepostojece sate1', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",7,8,"Srijeda");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            if(tabela.rows[0].cells[1].innerText == "07:00") brojac++;
                
            assert.equal(brojac, 0,"Greška");
        })
        it('Da li je moguce dodati aktivnost u nepostojece sate2', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",19,22,"Srijeda");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            if(tabela.rows[0].cells[12].innerText == "22:00") brojac++;
                
            assert.equal(brojac, 0,"Greška");
        })
        it('Preklapanje termina1', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",16,18,"Četvrtak");
            Raspored.dodajAktivnost(okvir,"ASP","tutorijal",15,17,"Četvrtak");
            
            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            for(var i=0;i<tabela.rows[4].cells.length;i++)
            if(tabela.rows[4].cells[i].innerText == ("DM" + "\n" + "tutorijal")) brojac++;
                
            assert.equal(brojac, 1,"Greška");
        })
        it('Preklapanje termina2', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"RMA","vježbe",12.5,14,"Srijeda");
            Raspored.dodajAktivnost(okvir,"DM","tutorijal",13.5,16,"Srijeda");
            Raspored.dodajAktivnost(okvir,"OR","vježbe",11,13,"Srijeda");

            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            for(var i=0;i<tabela.rows[3].cells.length;i++)
            if(tabela.rows[3].cells[i].innerText == ("RMA" + "\n" + "vježbe")) brojac++;
                
            assert.equal(brojac, 1,"Greška");
        })
        it('Kada početak i kraj nastave nisu puni sat', () =>{
            let okvir = document.getElementById("okvir7");
            Raspored.dodajAktivnost(okvir,"RMA","vježbe",12.5,14.5,"Utorak");

            var tabela = okvir.getElementsByTagName("table")[0];
            var brojac = 0;

            for(var i=0;i<tabela.rows[1].cells.length;i++)
            if(tabela.rows[1].cells[i].innerText == ("RMA" + "\n" + "vježbe")) brojac++;
                
            assert.equal(brojac, 0,"Greška");
        })
    });
});
