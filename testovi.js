const xhr = require("xmlhttprequest").XMLHttpRequest;

let testovi = (function() {
    function ObrisiPredmeteIAktivnostiImpl(callback) {
        var ajaxObrisiPredmeteIAktivnosti = new xhr();
        ajaxObrisiPredmeteIAktivnosti.open('DELETE', 'http://localhost:3000/all', true);
        ajaxObrisiPredmeteIAktivnosti.onreadystatechange = function() {
            if (ajaxObrisiPredmeteIAktivnosti.readyState == 4 && ajaxObrisiPredmeteIAktivnosti.status == 200) {
                var porukaBrisanja = JSON.parse(this.responseText).message;
                callback(porukaBrisanja);
            }
        }
        ajaxObrisiPredmeteIAktivnosti.send();
    }

    function ObrisiPredmetImpl(nazivPredmeta,callback) {
        var ajaxObrisiPredmet = new xhr();
        ajaxObrisiPredmet.open('DELETE', 'http://localhost:3000/predmet/' + nazivPredmeta, true);
        ajaxObrisiPredmet.onreadystatechange = function() {
            if (ajaxObrisiPredmet.readyState == 4 && ajaxObrisiPredmet.status == 200) {
                var porukaBrisanja = JSON.parse(this.responseText).message;
                callback(porukaBrisanja);
            }
        }
        ajaxObrisiPredmet.send();
    }

    function ObrisiAktivnostImpl(nazivPredmeta,callback) {
        var ajaxObrisiAktivnost = new xhr();
        ajaxObrisiAktivnost.open('DELETE', 'http://localhost:3000/aktivnost/' + nazivPredmeta, true);
        ajaxObrisiAktivnost.onreadystatechange = function() {
            if (ajaxObrisiAktivnost.readyState == 4 && ajaxObrisiAktivnost.status == 200) {
                var porukaBrisanja = JSON.parse(this.responseText).message;
                callback(porukaBrisanja);
            }
        }
        ajaxObrisiAktivnost.send();
    }

    function DodajPredmetImpl(data,callback) {
        var predmetJSON =  JSON.stringify(data);
    
        var ajaxDodajPredmet = new xhr();
        ajaxDodajPredmet.open('POST', 'http://localhost:3000/predmet', true);
        ajaxDodajPredmet.setRequestHeader("Content-Type", "application/json");
        ajaxDodajPredmet.getResponseHeader("Content-Type", "application/json");
        ajaxDodajPredmet.onreadystatechange = function() {
            if (ajaxDodajPredmet.readyState == 4 && ajaxDodajPredmet.status == 200) {
                var porukaPredmet = JSON.parse(this.responseText).message;
                callback(porukaPredmet);
            }
        }
        ajaxDodajPredmet.send(predmetJSON);
    }

    function DodajAktivnostImpl(data,callback) {
        var aktivnostJSON =  JSON.stringify(data);
    
        var ajaxDodajAktivnost = new xhr();
        ajaxDodajAktivnost.open('POST', 'http://localhost:3000/aktivnost', true);
        ajaxDodajAktivnost.setRequestHeader("Content-Type", "application/json");
        ajaxDodajAktivnost.getResponseHeader("Content-Type", "application/json");
        ajaxDodajAktivnost.onreadystatechange = function() {
            if (ajaxDodajAktivnost.readyState == 4 && ajaxDodajAktivnost.status == 200) {
                var porukaAktivnosti = JSON.parse(this.responseText).message;
                callback(porukaAktivnosti);
            }
        }
        ajaxDodajAktivnost.send(aktivnostJSON);
    }

    function ZaNazivVracaListuAktivnostiImpl(nazivPredmeta,callback) {
        var ajaxNazivPredmetAktivnosti = new xhr();
        ajaxNazivPredmetAktivnosti.open("GET", "http://localhost:3000/predmet/" + nazivPredmeta + "/aktivnost", true);
        ajaxNazivPredmetAktivnosti.onreadystatechange = function() {
            if (ajaxNazivPredmetAktivnosti.readyState == 4 && ajaxNazivPredmetAktivnosti.status == 200){
                var aktivnosti = JSON.parse(this.responseText);
                callback(aktivnosti);      
            }
        }
        ajaxNazivPredmetAktivnosti.send();
    }

    function PredmetiImpl(callback) {
        var ajaxPredmet = new xhr();
        ajaxPredmet.open("GET", "http://localhost:3000/predmeti", true);
        ajaxPredmet.onreadystatechange = function() {
            if (ajaxPredmet.readyState == 4 && ajaxPredmet.status == 200){
                var predmeti = JSON.parse(this.responseText);
                callback(predmeti);      
            }
        }
        ajaxPredmet.send();
    }

    function AktivnostiiImpl(callback) {
        var ajaxAktivnosti = new xhr();
        ajaxAktivnosti.open("GET", "http://localhost:3000/aktivnosti", true);
        ajaxAktivnosti.onreadystatechange = function() {
            if (ajaxAktivnosti.readyState == 4 && ajaxAktivnosti.status == 200){
                var aktivnosti = JSON.parse(this.responseText);
                callback(aktivnosti);      
            }
        }
        ajaxAktivnosti.send();
    }

    return {
        ObrisiPredmeteIAktivnosti: ObrisiPredmeteIAktivnostiImpl,
        ObrisiPredmet: ObrisiPredmetImpl,
        ObrisiAktivnost: ObrisiAktivnostImpl,
        DodajPredmet: DodajPredmetImpl,
        DodajAktivnost: DodajAktivnostImpl,
        ZaNazivVracaListuAktivnosti: ZaNazivVracaListuAktivnostiImpl,
        Predmeti: PredmetiImpl,
        Aktivnosti: AktivnostiiImpl
    }
}());

module.exports = testovi;