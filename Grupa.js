const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const Grupa = sequelize.define("grupas", {
        naziv : Sequelize.STRING
    })
    return Grupa;
} 