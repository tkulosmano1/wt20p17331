const sinon = require('sinon');
const fs = require('fs');
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();

let testovi = require('./testovi');

function DodajPredmet(tekst) {
    it("DodajPredmetTest()", function(done) {
        var test = [...tekst.split(',')];
        var data = JSON.parse(test[2]);
        var poruka = JSON.parse(test[3]).message;
        
        testovi.DodajPredmet(data,function(rezultat) {
            console.log(rezultat);
            console.log(poruka);
            rezultat.should.deep.equal(poruka);
            done();
        });
    });
}

function DodajAktivnost(tekst) {
    it("DodajAktivnostTest()", function(done) {
        var test = tekst.replace("POST,/aktivnost,{","");
        test = [...test.split("},{")];
        var aktivnost = JSON.parse("{"+test[0]+"}");
        var porukaAktivnosti = JSON.parse("{"+test[1].split('}')[0]+"}").message;
        testovi.DodajAktivnost(aktivnost,function(rezultat) {
            console.log(rezultat);
            console.log(porukaAktivnosti);
            rezultat.should.deep.equal(porukaAktivnosti);
            done();
        });
    });
}

function ObrisiSve(tekst) {
    it("BrisanjeSveTest()", function(done){
        var test = [...(tekst.split(','))];
        var porukaBrisanja = JSON.parse(test[3]).message;
        testovi.ObrisiPredmeteIAktivnosti(function(rezultat){
            console.log(rezultat);
            console.log(porukaBrisanja);
            rezultat.should.deep.equal(porukaBrisanja);
            done();
        });      
    });
}

function ObrisiPredmet(tekst) {
    it("BrisanjePredmetaTest()", function(done) {
        tekst = tekst.replace("DELETE,/predmet,","");
        var aktivnosti = [...tekst.split(",")];

        var predmet = JSON.parse(aktivnosti[0]).naziv;
        var poruka = JSON.parse(aktivnosti[1]).message;
        
        testovi.ObrisiPredmet(predmet,function(rezultat){
            console.log(rezultat);
            console.log(poruka);
            rezultat.should.deep.equal(poruka);
            done();
        });
    })
}

function ObrisiAktivnost(tekst) {
    it("BrisanjeAktivnostiTest()", function(done) {
        tekst = tekst.replace("DELETE,/aktivnost,","");
        var aktivnosti = [...tekst.split(",")];

        var predmet = JSON.parse(aktivnosti[0]).naziv;
        var poruka = JSON.parse(aktivnosti[1]).message;
        
        testovi.ObrisiAktivnost(predmet,function(rezultat){
            console.log(rezultat);
            console.log(poruka);
            rezultat.should.deep.equal(poruka);
            done();
        });
    })
}

function VratiPraznuListuPredmeta(tekst) {
    it("PredmetiPraznaListaTest()", function(done){
        var test = [...tekst.split(',')];
        
        testovi.Predmeti(function(rezultat){
            console.log(JSON.parse(test[3]));
            console.log(rezultat);
            rezultat.should.deep.equal(JSON.parse(test[3]));
            done();
        });
    });
}

function VratiPraznuListuAktivnosti(tekst) {
    it("AktivnostiPraznaListaTest()", function(done){
        var test = [...tekst.split(',')];
        
        testovi.Aktivnosti(function(rezultat){
            console.log(JSON.parse(test[3]));
            console.log(rezultat);
            rezultat.should.deep.equal(JSON.parse(test[3]));
            done();
        });
    });
}

function VratiListuPredmeta(tekst) {
    it("PredmetiListaTest()", function(done){
        var test = [...(tekst.split('['))];
        test = [...test[1].split(']')];
        test = [...test[0].split(',')];
        var predmeti = [];
        
        for(var i = 0; i < test.length; i++) 
            predmeti.push(JSON.parse(test[i]));
        
        testovi.Predmeti(function(rezultat){
            console.log(rezultat);
            console.log(predmeti);
            rezultat.should.deep.equal(predmeti);
            done();
        });
    });
}

function VratiListuAktivnosti(tekst) {
    it("AktivnostiListaTest()", function(done){
        tekst = tekst.replace("GET,/aktivnosti,null,[","");
        var aktivnosti = tekst.replace("]","");
        aktivnosti = aktivnosti.replace("},{","} {");
        aktivnosti = [...aktivnosti.split(" ")];

        var aktivnostiLista = [];

        for(var i = 0; i < aktivnosti.length; i++) 
            aktivnostiLista.push(JSON.parse(aktivnosti[i]));
        
        testovi.Aktivnosti(function(rezultat){
            console.log(rezultat);
            console.log(aktivnostiLista);
            rezultat.should.deep.equal(aktivnostiLista);
            done();
        });
    });
}

function VratListuAktivnostiZaPredmet(tekst) {
    it("ZaPredmetAktivnostiListaTest()", function(done){
        tekst = tekst.replace("GET,/predmet,","");
        var aktivnosti = tekst.replace("]","");
        aktivnosti = aktivnosti.replace(",["," ");
        aktivnosti = [...aktivnosti.split(" ")];

        var predmet = JSON.parse(aktivnosti[0]).naziv;
        var aktivnostiLista = [];

        for(var i = 1; i < aktivnosti.length; i++) 
            aktivnostiLista.push(JSON.parse(aktivnosti[i]));
        
        testovi.ZaNazivVracaListuAktivnosti(predmet,function(rezultat){
            console.log(rezultat);
            console.log(aktivnostiLista);
            rezultat.should.deep.equal(aktivnostiLista);
            done();
        });
    });
}

describe("Testovi za rute", function() {
    var tekst = fs.readFileSync('testniPodaci.txt','utf-8').toString().replace(/\\/g,"");
    var tekst = [...tekst.split('\r\n')];

    beforeEach(function() {
       this.xhr = sinon.useFakeXMLHttpRequest();
       this.requests = [];
       this.xhr.onCreate = function(xhr) {
           this.requests.push(xhr);
       }.bind(this); 
    });

    afterEach(function() {
        this.xhr.restore();
    });

    for(var i = 0; i < tekst.length; i++) {
        if(tekst[i].includes("/predmet") && tekst[i].includes("POST")) {
            DodajPredmet(tekst[i]);
        }
        if(tekst[i].includes("/aktivnost") && tekst[i].includes("POST")) {
            DodajAktivnost(tekst[i]);
        }
        if(tekst[i].includes("/all") && tekst[i].includes("DELETE")) {
            ObrisiSve(tekst[i]);
        }
        if(tekst[i].includes("/predmet") && tekst[i].includes("DELETE")) {
            ObrisiPredmet(tekst[i]);
        }
        if(tekst[i].includes("/aktivnost") && tekst[i].includes("DELETE")) {
            ObrisiAktivnost(tekst[i]);
        }
        if(tekst[i].includes("/predmeti") && tekst[i].includes("GET") && tekst[i].includes("[]")) {
            VratiPraznuListuPredmeta(tekst[i]);
        }
        if(tekst[i].includes("/predmeti") && tekst[i].includes("GET") && tekst[i].includes("[{")) {
            VratiListuPredmeta(tekst[i]);
        }
        if(tekst[i].includes("/aktivnosti") && tekst[i].includes("GET") && tekst[i].includes("[]")) {
            VratiPraznuListuAktivnosti(tekst[i]);
        }
        if(tekst[i].includes("/aktivnosti") && tekst[i].includes("GET") && tekst[i].includes("[{")) {
            VratiListuAktivnosti(tekst[i]);
        }
        if(tekst[i].includes("/predmet,") && tekst[i].includes("GET")) {
            VratListuAktivnostiZaPredmet(tekst[i]);
        }
    }
})