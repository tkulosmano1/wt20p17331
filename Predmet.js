const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const Predmet = sequelize.define("predmets", {
        naziv : Sequelize.STRING
    })
    return Predmet;
} 