const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const Tip = sequelize.define("tips", {
        naziv : Sequelize.STRING
    })
    return Tip;
} 