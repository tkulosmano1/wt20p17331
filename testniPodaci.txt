DELETE,/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}
POST,/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Uspješno dodan predmet!\"}
POST,/predmet,{\"naziv\":\"WT\"},{\"message\":\"Uspješno dodan predmet!\"}
POST,/aktivnost,{\"naziv\":\"WT\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Utorak\"},{\"message\": \"Uspješno dodana aktivnost!\"}
POST,/aktivnost,{\"naziv\":\"RMA\",\"tip\":\"Predavanje\",\"pocetak\":15.4,\"kraj\":18,\"dan\":\"Srijeda\"},{\"message\": \"Aktivnost nije validna!\"}
POST,/aktivnost,{\"naziv\":\"RMA\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Srijeda\"},{\"message\": \"Uspješno dodana aktivnost!\"}
GET,/aktivnosti,null,[{\"naziv\":\"WT\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Utorak\"},{\"naziv\":\"RMA\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Srijeda\"}]
GET,/predmeti,null,[{\"naziv\":\"RMA\"},{\"naziv\":\"WT\"}]
GET,/predmet,{\"naziv\":\"WT\"},[{\"naziv\":\"WT\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Utorak\"}]
GET,/predmet,{\"naziv\":\"RMA\"},[{\"naziv\":\"RMA\",\"tip\":\"Predavanje\",\"pocetak\":15,\"kraj\":18,\"dan\":\"Srijeda\"}]
DELETE,/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Uspješno obrisan predmet!\"}
DELETE,/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Greška - predmet nije obrisan!\"}
DELETE,/aktivnost,{\"naziv\":\"RMA\"},{\"message\":\"Uspješno obrisana aktivnost!\"}
DELETE,/aktivnost,{\"naziv\":\"RMA\"},{\"message\":\"Greška - aktivnost nije obrisana!\"}
DELETE,/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}
GET,/predmeti,null,[]
GET,/aktivnosti,null,[]