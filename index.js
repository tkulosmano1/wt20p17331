const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const db = require('./db.js');
const app = express();

var listaPredmeta = [];
var listaAktivnosti = [];
var listaTip = [];
var listaDan = [];
var listaGrupa = [];
var listaStudent = [];

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//punjenje baze i kreiranje tabela
db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});;

function inicializacija(){
    var predmetiListaPromisea = [];
    var tipListaPromisea = [];
    var danListaPromisea = [];
    var grupaListaPromisea = [];
    var studentiListaPromisea = [];
    var studentiGrupaListaPromisea = [];
    return new Promise(function(resolve,reject){
        predmetiListaPromisea.push(db.Predmet.create({naziv:'RMA'}));
        predmetiListaPromisea.push(db.Predmet.create({naziv:'WT'}));
        tipListaPromisea.push(db.Tip.create({naziv:'Predavanje'}));
        tipListaPromisea.push(db.Tip.create({naziv:'Tutorijal'}));
        tipListaPromisea.push(db.Tip.create({naziv:'Vježbe'}));
        danListaPromisea.push(db.Dan.create({naziv:'Ponedjeljak'}));
        danListaPromisea.push(db.Dan.create({naziv:'Utorak'}));
        danListaPromisea.push(db.Dan.create({naziv:'Srijeda'}));
        danListaPromisea.push(db.Dan.create({naziv:'Četvrtak'}));
        danListaPromisea.push(db.Dan.create({naziv:'Petak'}));
        studentiListaPromisea.push(db.Student.create({ime: "Neko Nekić",index: "12345"}));
        studentiListaPromisea.push(db.Student.create({ime: "Četvrti Neko",index: "18009,"}));
        Promise.all(predmetiListaPromisea).then(function(predmeti){
            var rma = predmeti.filter(function(r){return r.naziv==='RMA'})[0];
            var wt = predmeti.filter(function(r){return r.naziv==='WT'})[0];
            
            grupaListaPromisea.push(
                db.Grupa.create({naziv: 'RMAgrupa1', predmetId: rma.id}).then(function(k){
                    return new Promise(function(resolve,reject){resolve(k);});
                })
            );

            grupaListaPromisea.push(
                db.Grupa.create({naziv: 'WTgrupa1',predmetId: wt.id}).then(function(k){
                    return new Promise(function(resolve,reject){resolve(k);});
                })
            );

            grupaListaPromisea.push(
                db.Grupa.create({naziv: 'WTgrupa2', predmetId: wt.id}).then(function(k){
                    return new Promise(function(resolve,reject){resolve(k);});
                })
            );
            Promise.all(studentiListaPromisea).then(function(studenti){
                var neko = studenti.filter(function(r){return r.ime==='Neko Nekić'})[0];
                var cetvrti = studenti.filter(function(r){return r.ime==='Četvrti Neko'})[0];
                Promise.all(grupaListaPromisea).then(function(grupa){
                    var rma1 = grupa.filter(function(r){return r.naziv==='RMAgrupa1'})[0];
                    var wt1 = grupa.filter(function(r){return r.naziv==='WTgrupa1'})[0];
                    
                    studentiGrupaListaPromisea.push(
                        db.StudentGrupa.create({studentId: neko.id, grupaId: wt1.id}).then(function(k){
                            return new Promise(function(resolve,reject){resolve(k);});
                        })
                    );
                    
                    studentiGrupaListaPromisea.push(
                        db.StudentGrupa.create({studentId: cetvrti.id, grupaId: rma1.id}).then(function(k){
                            return new Promise(function(resolve,reject){resolve(k);});
                        })
                    );
                });
            });
        });
    })
}

//CRUD za studenta

function ListaStudent() {
    db.Student.findAll().then(function(studenti){
        listaStudent = [];
        
        if(studenti.length != 0) {
            for (var i = 0; i < studenti.length; i++) {
                var student = studenti[i];
                listaStudent.push({ime: student.ime, index: student.index});
            }     
        }
    });
}

app.get('/v2/studenti', function(req,res) {
    
    db.Student.findAll().then(function(studenti){
        listaStudent = [];
        
        if(studenti.length != 0) {
            for (var i = 0; i < studenti.length; i++) {
                var student = studenti[i];
                listaStudent.push({ime: student.ime, index: student.index});
            }     
        }
        res.json(listaStudent);
    });
});

app.post('/v2/student',function(req,res){
    let tijelo = req.body;
    let studentiJSON = {ime: tijelo['ime'].toUpperCase(), index: tijelo['index']};
    if(listaStudent.length == 0 && studentiJSON.ime != "" && studentiJSON.index != "") {
        db.Student.findOrCreate({where:{ime:studentiJSON.ime, index:studentiJSON.index}}).then(function(result){
            listaStudent.push(studentiJSON);
            res.json({message: "Uspješno dodan student!"});
        });
    }
    else if(studentiJSON.ime != "" && studentiJSON.index != "") {
        var brojac = 0;
        for(var i = 0; i < listaStudent.length; i++)
            if(studentiJSON.ime == listaStudent[i].ime || studentiJSON.index == listaStudent[i].index)
                brojac++;
        if(brojac == 0) {
            db.Student.findOrCreate({where:{ime:studentiJSON.ime, index:studentiJSON.index}}).then(function(result){
                listaStudent.push(studentiJSON);
                res.json({message: "Uspješno dodan student!"});
            });
        }
        else 
            res.json({message: "Greška - studenta nije moguće dodati!"});
    }
});

app.delete('/v2/student/:index', function(req,res) {
    var student = {index: req.params.index};
    var studenti = JSON.stringify(listaStudent).toString();
    if(studenti.includes(student.index)) {
        db.Student.findOne({where:{index:student.index}}).then(function(student){
            var studentId = student.id;
            db.StudentGrupa.destroy({where:{studentId:studentId}}).then(function(result){
                db.Student.destroy({where:{index:student.index}}).then(function(result){
                    ListaStudent();
                    res.json({message:"Uspješno obrisan student!"});
                });
            });
        });
    }
    else {
        res.json({message:"Greška - student nije obrisan!"});
    }
});

app.put('/v2/student/:id', function(req,res) {
    let tijelo = req.body;
    let studentiJSON = {ime: tijelo['ime'].toUpperCase(), index: tijelo['index']};
    var studentId = req.params.id;

    if(studentiJSON.ime != "" && studentiJSON.index != "") {
        var brojac = 0;
        for(var i = 0; i < listaStudent.length; i++)
            if(studentiJSON.ime == listaStudent[i].ime || studentiJSON.index == listaStudent[i].index)
                brojac++;
        if(brojac == 0) {
            db.Student.update(studentJSON,{where:{id:studentId}}).then(function(result){
                ListaStudent();
                res.json({message: "Uspješno izmjenjen student!"});         
            });
        }
        else 
            res.json({message: "Greška - studenta nije moguće promjeniti!"});
    }
});

//CRUD za grupu
function Grupe(grupa,predmetId,callback) {
    var predmetNaziv = ""; 
    
    db.Predmet.findById(predmetId).then(function(predmet){
        predmetNaziv = predmet.naziv;
        listaGrupa.push({naziv: grupa.naziv,predmet: predmetNaziv});
        callback(listaGrupa.length);
    });
}

function ListaGrupa() {
    db.Grupa.findAll().then(function(grupe){
        listaGrupa = [];
        
        if(grupe.length != 0) {
            for (var i = 0; i < grupe.length; i++) {
                var predmetId = grupe[i].predmetId;
                var grupa = grupe[i];
                Grupe(grupa,predmetId,function(broj){
                    
                });
            }     
        }
    })
}

app.get('/v2/grupe', function(req,res) {
    db.Grupa.findAll().then(function(grupe){
        listaGrupa = [];
        
        if(grupe.length != 0) {
            for (var i = 0; i < grupe.length; i++) {
                var predmetId = grupe[i].predmetId;
                var grupa = grupe[i];
                Grupe(grupa,predmetId,function(broj){
                    if(grupe.length == broj) {
                        res.json(listaGrupa);
                    }
                });
            }     
        }
        else
            res.json(listaGrupa);
    })
});

app.post('/v2/grupa',function(req,res){
    let tijelo = req.body;
    let grupaJSON = {naziv: tijelo['naziv'].toUpperCase(), predmet: tijelo['predmet']};
    if(listaGrupa.length == 0 && grupaJSON.naziv != "") {
        db.Predmet.findOne({where:{naziv: grupaJSON.predmet}}).then(function(predmet){
            var predmetId = predmet.id;
            db.Grupa.findOrCreate({where:{naziv:grupaJSON.naziv,predmetId:predmetId}}).then(function(result){
                listaGrupa.push(grupaJSON);
                res.json({message: "Uspješno dodana grupa!"});
            });
        });
    }
    else if(grupaJSON.naziv != "") {
        var brojac = 0;
        for(var i = 0; i < listaGrupa.length; i++)
            if(grupaJSON.naziv == listaGrupa.naziv)
                brojac++;
        if(brojac == 0) {
            db.Predmet.findOne({where:{naziv: grupaJSON.predmet}}).then(function(predmet){
                var predmetId = predmet.id;
                db.Grupa.findOrCreate({where:{naziv:grupaJSON.naziv,predmetId:predmetId}}).then(function(result){
                    listaGrupa.push(grupaJSON);
                    res.json({message: "Uspješno dodana grupa!"});
                });
            });
        }
        else 
            res.json({message: "Grupa sa tim nazivom postoji!"});
    }
});

app.delete('/v2/grupa/:naziv', function(req,res) {
    var grupa = {naziv: req.params.naziv};
    var grupe = JSON.stringify(listaGrupa).toString();
    if(grupe.includes(grupa.naziv)) {
        var index = listaGrupa.indexOf(grupa);
        listaPredmeta.splice(index,1);
        db.Grupa.findOne({where:{naziv:grupa.naziv}}).then(function(grupa){
            var grupaId = grupa.id;
            db.StudentGrupa.destroy({where:{grupaId:grupaId}}).then(function(result){
                db.Grupa.destroy({where:{naziv:grupa.naziv}}).then(function(result){
                    res.json({message:"Uspješno obrisana grupa!"});
                });
            });
        });
    }
    else {
        res.json({message:"Greška - grupa nije obrisana!"});
    }
});

app.put('/v2/grupa/:id', function(req,res) {
    let tijelo = req.body;
    let grupaJSON = {naziv: tijelo['naziv'].toUpperCase(), predmet: tijelo['predmet']};
    var grupaId = req.params.id;
    var grupe = JSON.stringify(listaGrupa).toString();

    if(!grupe.includes(grupaJSON.naziv) && grupaJSON.naziv != "") {
        db.Predmet.findOne({where:{naziv:grupaJSON.predmet}}).then(function(predmet){
            var predmetId = predmet.id;
            grupaJSON = {naziv: grupaJSON.naziv,predmetId: predmetId};
            db.Grupa.update(grupaJSON,{where:{id:grupaId}}).then(function(result){
                ListaGrupa();
                res.json({message: "Uspješno izmjenjena grupa!"});         
            });
        });
    }
    else 
        res.json({message: "Grupa nije izmjenjen!"});
});


//CRUD za predmet
function ListaPredmet() {
    db.Predmet.findAll().then(function(predmeti){
        listaPredmeta = [];
        
        if(predmeti.length != 0) {
            for (var i = 0; i < predmeti.length; i++) {
                listaPredmeta.push(predmeti[i].naziv);
            }     
        }
    });    
}
app.get('/v2/predmeti', function(req,res) {
    db.Predmet.findAll().then(function(predmeti){
        listaPredmeta = [];
        var predmetiJSON = [];
        if(predmeti.length != 0) {
            for (var i = 0; i < predmeti.length; i++) {
                predmetiJSON.push({naziv: predmeti[i].naziv});
                listaPredmeta.push(predmeti[i].naziv);
            }     
        }
        res.json(predmetiJSON);
    });
});

app.post('/v2/predmet',function(req,res){
    let tijelo = req.body;
    let predmet = tijelo['naziv'].toUpperCase();
    if(listaPredmeta.length == 0 && predmet != "") {
        db.Predmet.findOrCreate({where:{naziv:predmet}}).then(function(result){
            listaPredmeta.push(predmet);
            res.json({message: "Uspješno dodan predmet!"});
        });
    }
    else if(!listaPredmeta.includes(predmet) && predmet != "") {
        db.Predmet.findOrCreate({where:{naziv:predmet}}).then(function(result){
            listaPredmeta.push(predmet);
            res.json({message: "Uspješno dodan predmet!"});
        });
    }
    else {
        res.json({message: "Naziv predmeta postoji!"});
    }
});

app.delete('/v2/predmet/:naziv', function(req,res) {
    var predmet = req.params.naziv;
    if(listaPredmeta.includes(predmet)) {
        var index = listaPredmeta.indexOf(predmet);
        listaPredmeta.splice(index,1);
        db.Predmet.findOne({where:{naziv:predmet}}).then(function(predmet){
            var predmetId = predmet.id;
            db.Aktivnost.destroy({where:{predmetId:predmetId}}).then(function(result){
                ListaAktivnost();
                db.Grupa.destroy({where:{predmetId:predmetId}}).then(function(result){
                    ListaGrupa();
                    db.Predmet.destroy({where:{id:predmetId}}).then(function(result){
                        res.json({message:"Uspješno obrisan predmet!"});
                    });
                });
            });
        });
    }
    else {
        res.json({message:"Greška - predmet nije obrisan!"});
    }
});

app.put('/v2/predmet/:id', function(req,res) {
    let tijelo = req.body;
    let predmet = {naziv:tijelo['naziv'].toUpperCase()};
    var predmetId = req.params.id;
    var predmetJSON = {naziv:predmet};
    
    if(!listaPredmeta.includes(predmet) && predmet != "") {
        db.Predmet.update(predmetJSON,{where:{id:predmetId}}).then(function(result){
            ListaPredmet();
            ListaGrupa();
            db.Aktivnost.update(predmetJSON,{where:{predmetId:predmetId}}).then(function(){
                ListaAktivnost();
                res.json({message: "Uspješno izmjenjen predmet!"});         
            });
        });
    }
    else 
        res.json({message: "Naziv predmeta nije izmjenjen!"});
});

//CRUD za tip
function ListaTip(){
    db.Tip.findAll().then(function(tip){
        listaTip = [];
        if(tip.length != 0) {
            for (var i = 0; i < tip.length; i++) {
                listaTip.push(tip[i].naziv);
            }     
        }
    })    
}
app.get('/v2/tipovi', function(req,res) {
    db.Tip.findAll().then(function(tip){
        listaTip = [];
        var tipJSON = [];
        if(tip.length != 0) {
            for (var i = 0; i < tip.length; i++) {
                tipJSON.push({naziv: tip[i].naziv});
                listaTip.push(tip[i].naziv);
            }     
        }
        res.json(tipJSON);
    })
});

app.post('/v2/tip',function(req,res){
    let tijelo = req.body;
    let tip = tijelo['naziv'].toUpperCase();
    if(listaTip.length == 0 && tip != "") {
        db.Tip.findOrCreate({where:{naziv:tip}}).then(function(result){
            listaTip.push(tip);
            res.json({message: "Uspješno dodan tip!"});
        });
    }
    else if(!listaTip.includes(tip) && tip != "") {
        db.Tip.findOrCreate({where:{naziv:tip}}).then(function(result){
            listaTip.push(tip);
            res.json({message: "Uspješno dodan tip!"});
        });
    }
    else {
        res.json({message: "Naziv tipa postoji!"});
    }
});

app.delete('/v2/tip/:naziv', function(req,res) {
    var tip = req.params.naziv;
    if(listaTip.includes(tip)) {
        var index = listaTip.indexOf(tip);
        listaTip.splice(index,1);
        db.Tip.findOne({where:{naziv:tip}}).then(function(tip){
            var tipId = tip.id;
            db.Aktivnost.destroy({where:{tipId:tipId}}).then(function(result){
                ListaAktivnost();
                console.log(listaAktivnosti);
                db.Tip.destroy({where:{id:tipId}}).then(function(result){
                    res.json({message:"Uspješno obrisan tip!"});
                })
            })
        })
    }
    else {
        res.json({message:"Greška - tip nije obrisan!"});
    }
});

app.put('/v2/tip/:id', function(req,res) {
    let tijelo = req.body;
    let tipJSON = {naziv:tijelo['naziv'].toUpperCase()};
    var tipId = req.params.id;
    
    if(!listaTip.includes(tipJSON.naziv) && tipJSON.naziv != "") {
        db.Tip.update(tipJSON,{where:{id:tipId}}).then(function(result){
            ListaTip();
            res.json({message: "Uspješno izmjenjen tip!"});         
        });
    }
    else 
        res.json({message: "Naziv tipa nije izmjenjen!"});
});

//CRUD za dan
function ListaDan(){
    db.Dan.findAll().then(function(dan){
        listaDan = [];
        if(dan.length != 0) {
            for (var i = 0; i < dan.length; i++) {
                listaDan.push(dan[i].naziv);
            }     
        }
        res.json(danJSON);
    })
}
app.get('/v2/dani', function(req,res) {
    db.Dan.findAll().then(function(dan){
        listaDan = [];
        var danJSON = [];
        if(dan.length != 0) {
            for (var i = 0; i < dan.length; i++) {
                danJSON.push({naziv: dan[i].naziv});
                listaDan.push(dan[i].naziv);
            }     
        }
        res.json(danJSON);
    })
});

app.post('/v2/dan',function(req,res){
    let tijelo = req.body;
    let dan = tijelo['naziv'].toUpperCase();
    if(listaDan.length == 0 && dan != "") {
        db.Dan.findOrCreate({where:{naziv:dan}}).then(function(result){
            listaDan.push(dan);
            res.json({message: "Uspješno dodan dan!"});
        });
    }
    else if(!listaDan.includes(dan) && dan != "") {
        db.Dan.findOrCreate({where:{naziv:dan}}).then(function(result){
            listaDan.push(dan);
            res.json({message: "Uspješno dodan dan!"});
        });
    }
    else {
        res.json({message: "Dan postoji!"});
    }
});

app.delete('/v2/dan/:naziv', function(req,res) {
    var dan = req.params.naziv;
    if(listaDan.includes(dan)) {
        var index = listaDan.indexOf(dan);
        listaDan.splice(index,1);
        db.Dan.findOne({where:{naziv:dan}}).then(function(dan){
            var danId = dan.id;
            db.Aktivnost.destroy({where:{danId:danId}}).then(function(result){
                ListaAktivnost();
                db.Dan.destroy({where:{id:danId}}).then(function(result){
                    console.log(listaAktivnosti);
                    res.json({message:"Uspješno obrisan dan!"});
                })
            })
        })
    }
    else {
        res.json({message:"Greška - dan nije obrisan!"});
    }
});

app.put('/v2/dan/:id', function(req,res) {
    let tijelo = req.body;
    let danJSON = {naziv:tijelo['naziv'].toUpperCase()};
    var danId = req.params.id;
    
    if(!listaDan.includes(danJSON.naziv) && danJSON.naziv != "") {
        db.Dan.update(danJSON,{where:{id:danId}}).then(function(result){
            ListaDan();
            res.json({message: "Uspješno izmjenjen dan!"});         
        });
    }
    else 
        res.json({message: "Naziv dana nije izmjenjen!"});
});

//CRUD za aktivnost
function ListaAktivnost() {
    db.Aktivnost.findAll().then(function(aktivnosti){
        listaAktivnosti = [];
        
        if(aktivnosti.length != 0) {
            for(var i = 0; i < aktivnosti.length; i++) {
                var tipId = aktivnosti[i].tipId;
                var danId = aktivnosti[i].danId;
                var aktivnost = aktivnosti[i];
                Aktivnosti(aktivnost,tipId,danId,function(broj){
                    if(aktivnosti.length == broj)
                        console.log(listaAktivnosti);
                });
            }
        }
    });
}
function Aktivnosti(aktivnost,tipId,danId,callback) {
    var tipNaziv = ""; 
    var danNaziv = "";
    
    db.Tip.findById(tipId).then(function(tip){
        tipNaziv = tip.naziv;
        db.Dan.findById(danId).then(function(dan){
            danNaziv = dan.naziv;
            listaAktivnosti.push({naziv: aktivnost.naziv,tip: tipNaziv,pocetak: aktivnost.pocetak,kraj: aktivnost.kraj,dan: danNaziv});
            callback(listaAktivnosti.length);
        });
    });
}

app.get('/v2/aktivnosti', function(req, res){
    db.Aktivnost.findAll().then(function(aktivnosti){
        listaAktivnosti = [];
        
        if(aktivnosti.length != 0) {
            for(var i = 0; i < aktivnosti.length; i++) {
                var tipId = aktivnosti[i].tipId;
                var danId = aktivnosti[i].danId;
                var aktivnost = aktivnosti[i];
                Aktivnosti(aktivnost,tipId,danId,function(broj){
                    if(aktivnosti.length == broj) {
                        res.json(listaAktivnosti);
                    }
                });
            }
        }
        else {
            res.json(listaAktivnosti);
        }
    });
});

app.post('/v2/aktivnost',function(req,res){
    let tijelo = req.body;
    var novaAktivnostJSON = {naziv: tijelo['naziv'], tip: tijelo['tip'], pocetak: parseFloat(tijelo['pocetak']),
                             kraj: parseFloat(tijelo['kraj']), dan: tijelo['dan']};
    if(!(novaAktivnostJSON.pocetak >= 8 && novaAktivnostJSON.pocetak < 20) 
        || !(novaAktivnostJSON.kraj > 8 && novaAktivnostJSON.kraj <= 20) 
        || novaAktivnostJSON.pocetak >= novaAktivnostJSON.kraj
        || novaAktivnostJSON.pocetak % 0.5 != 0 || novaAktivnostJSON.kraj % 0.5 != 0
        || novaAktivnostJSON.naziv == "" || novaAktivnostJSON.pocetak == null || novaAktivnostJSON.kraj == null
        || novaAktivnostJSON.pocetak == "" || novaAktivnostJSON.kraj == ""){
        res.json({message: "Aktivnost nije validna!"});
    }
    else if(listaAktivnosti.length == 0) {
        db.Tip.findOne({where:{naziv:novaAktivnostJSON.tip}}).then(function(tip){
            var tipId = tip.id;
            db.Dan.findOne({where:{naziv:novaAktivnostJSON.dan}}).then(function(dan){
                var danId = dan.id;
                db.Predmet.findOne({where:{naziv:novaAktivnostJSON.naziv}}).then(function(predmet){
                    var predmetId = predmet.id;
                    db.Aktivnost.findOrCreate({where:{naziv: novaAktivnostJSON.naziv,pocetak: novaAktivnostJSON.pocetak,
                        kraj: novaAktivnostJSON.kraj, predmetId: predmetId, danId: danId, tipId: tipId}}).then(function(predmet){
                            listaAktivnosti.push(novaAktivnostJSON);
                            res.json({message: "Uspješno dodana aktivnost!"});
                    });
                });
            });
        }); 
    } 
    else{ 
        var brojac = 0;
        for(var i = 0; i < listaAktivnosti.length; i++) {
            if(listaAktivnosti[i].dan == novaAktivnostJSON.dan && 
               ((listaAktivnosti[i].pocetak <= novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj > novaAktivnostJSON.pocetak) ||
               (listaAktivnosti[i].pocetak < novaAktivnostJSON.kraj && listaAktivnosti[i].kraj >= novaAktivnostJSON.kraj) ||
               (listaAktivnosti[i].pocetak > novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj < novaAktivnostJSON.kraj))) 
                    brojac++;
        }
        if(brojac == 0) {
            db.Tip.findOne({where:{naziv:novaAktivnostJSON.tip}}).then(function(tip){
                var tipId = tip.id;
                db.Dan.findOne({where:{naziv:novaAktivnostJSON.dan}}).then(function(dan){
                    var danId = dan.id;
                    db.Predmet.findOne({where:{naziv:novaAktivnostJSON.naziv}}).then(function(predmet){
                        var predmetId = predmet.id;
                        db.Aktivnost.findOrCreate({where:{naziv: novaAktivnostJSON.naziv,pocetak: novaAktivnostJSON.pocetak,
                            kraj: novaAktivnostJSON.kraj, predmetId: predmetId, danId: danId, tipId: tipId}}).then(function(result){
                                listaAktivnosti.push(novaAktivnostJSON);
                                res.json({message: "Uspješno dodana aktivnost!"});
                        });
                    });
                });
            }); 
        }
        else
            res.json({message: "Aktivnost nije validna!"}); 
    }
});

app.delete('/v2/aktivnost/:naziv', function(req, res){
    var predmet = {naziv: req.params.naziv};
    var aktivnost = JSON.stringify(listaAktivnosti).toString();
    
    if(aktivnost.includes(predmet.naziv)) {
        db.Aktivnost.destroy({where:{naziv:predmet.naziv}}).then(function(result){
            ListaAktivnost();
            res.json({message:"Uspješno obrisana aktivnost!"});
        });
    }
    else {
        res.json({message:"Greška - aktivnost nije obrisana!"});
    }
});

app.put('/v2/aktivnost/:id', function(req,res){
    let tijelo = req.body;
    var aktivnostId = req.params.id;
    let novaAktivnostJSON = {naziv: tijelo['naziv'], tip: tijelo['tip'], pocetak: parseFloat(tijelo['pocetak']),
                            kraj: parseFloat(tijelo['kraj']), dan: tijelo['dan']};
    
    if(!(novaAktivnostJSON.pocetak >= 8 && novaAktivnostJSON.pocetak < 20) 
            || !(novaAktivnostJSON.kraj > 8 && novaAktivnostJSON.kraj <= 20) 
            || novaAktivnostJSON.pocetak >= novaAktivnostJSON.kraj
            || novaAktivnostJSON.pocetak % 0.5 != 0 || novaAktivnostJSON.kraj % 0.5 != 0
            || novaAktivnostJSON.naziv == "" || novaAktivnostJSON.pocetak == null || novaAktivnostJSON.kraj == null
            || novaAktivnostJSON.pocetak == "" || novaAktivnostJSON.kraj == ""){
                res.json({message: "Aktivnost nije validna!"});
    }
    
    var brojac = 0;
    for(var i = 0; i < listaAktivnosti.length; i++) {
        if(listaAktivnosti[i].dan == novaAktivnostJSON.dan && 
            ((listaAktivnosti[i].pocetak <= novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj > novaAktivnostJSON.pocetak) ||
            (listaAktivnosti[i].pocetak < novaAktivnostJSON.kraj && listaAktivnosti[i].kraj >= novaAktivnostJSON.kraj) ||
            (listaAktivnosti[i].pocetak > novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj < novaAktivnostJSON.kraj))) 
                brojac++;
    }
    if(brojac == 0) {
        db.Tip.findOne({where:{naziv:novaAktivnostJSON.tip}}).then(function(tip){
            var tipId = tip.id;
            db.Dan.findOne({where:{naziv:novaAktivnostJSON.dan}}).then(function(dan){
                var danId = dan.id;
                db.Predmet.findOne({where:{naziv:novaAktivnostJSON.naziv}}).then(function(predmet){
                    var predmetId = predmet.id;
                    var promjenjenaAktivnost = {naziv: novaAktivnostJSON.naziv,pocetak: novaAktivnostJSON.pocetak,
                        kraj: novaAktivnostJSON.kraj, predmetId: predmetId, danId: danId, tipId: tipId};
                    db.Aktivnost.update(promjenjenaAktivnost,{where:{id:aktivnostId}}).then(function(result){
                            ListaAktivnost();
                            res.json({message: "Uspješno promjenjena aktivnost!"});
                    });
                });
            });
        }); 
    }
    else
        res.json({message: "Aktivnost nije validna!"});
});

//post za student-grupa
function PromjeniGrupu(grupaId, promjeniGrupuStudentu) {
    db.StudentGrupa.findOne({where:{studentId: promjeniGrupuStudentu.studentId, grupaId: grupaId}}).then(function(studentGrupa){
        var studentGrupaId = studentGrupa.id;
        db.StudentGrupa.update(promjeniGrupuStudentu,{where:{id: studentGrupaId}}).then(function(result){

        });
    });
}
function PromjeniGrupuStudentu(promjeniGrupuStudentu,predmetId) {
    db.Grupa.findAll({where:{predmetId: predmetId}}).then(function(grupa){
        for (var i = 0; i < grupa.length; i++) {
            var grupaId = grupa[i].id;
            PromjeniGrupu(grupaId, promjeniGrupuStudentu);
        } 
    });
}

function DodajGrupuStudent(dodajGrupuStudent){
    db.StudentGrupa.findOrCreate({where:{studentId: dodajGrupuStudent.studentId, grupaId: dodajGrupuStudent.grupaId}}).then(function(result){
        
    });
}

function DodajStudenta(dodajStudenta,grupaId) {
    db.Student.findOrCreate({where:{ime: dodajStudenta.ime, index: dodajStudenta.index}}).then(function(result){
        db.Student.findAll().then(function(studenti){
            for(var i = 0; i < studenti.length; i++) {
                if(studenti[i].index == dodajStudenta.index) {
                    var dodajGrupuStudent = {studentId: studenti[i].id, grupaId: grupaId};
                    DodajGrupuStudent(dodajGrupuStudent);
                }
            }
        });
    });
}

app.post('/v2/studenti-grupa',function(req,res){
    var tijelo = req.body;
    var studentiGrupa = [];
    for(var i = 0; i < tijelo.length; i++){
        var student = tijelo[i];
        studentiGrupa.push({ime: student['ime'], index: student['index'], grupa: student['grupa']});
    }
    
    db.Grupa.findOne({where: {naziv: studentiGrupa[0].grupa}}).then(function(grupa){
        var grupaId = grupa.id;
        var predmetId = grupa.predmetId;
        db.Grupa.findAll().then(function(grupe){
            db.Student.findAll().then(function(studenti){
                db.StudentGrupa.findAll().then(function(studentiGrupaId){
                    var studentiGreska = [];
                    var studentiNisuUpisani = [];
                    var studentiUpisani = [];
                    var studentiPromjeniGrupu = [];
                    var studentiString = JSON.stringify(studenti).toString();
                    for(var i = 0; i < studentiGrupa.length; i++) {
                        if(studentiString.includes(studentiGrupa[i].ime) && studentiString.includes(studentiGrupa[i].index))
                            studentiUpisani.push(studentiGrupa[i]);
                        if(!studentiString.includes(studentiGrupa[i].ime) && studentiString.includes(studentiGrupa[i].index))
                            studentiGreska.push(studentiGrupa[i]);
                        if(!studentiString.includes(studentiGrupa[i].ime) && !studentiString.includes(studentiGrupa[i].index))
                            studentiNisuUpisani.push(studentiGrupa[i]);
                    }

                    for(var i = 0; i < studentiUpisani.length; i++) {
                        for(var j = 0; j < studenti.length; j++){
                            if(studentiUpisani[i].ime == studenti[j].ime) {
                                var studentId = studenti[j].id;
                                for(var k = 0; k < studentiGrupaId.length; k++) {
                                    if(studentId == studentiGrupaId[k].studentId && grupaId != studentiGrupaId[k].grupaId) {
                                        for(var n = 0; n < grupe.length; n++) {
                                            if(grupe[n].id == studentiGrupaId[k].grupaId && predmetId == grupe[n].predmetId) {
                                                studentiPromjeniGrupu.push(studenti[j]);
                                            }           
                                        }
                                    }   
                                } 
                            }
                        }
                    }

                    if(studentiPromjeniGrupu.length != 0) {
                        for(var i = 0; i < studentiPromjeniGrupu.length; i++) {
                            var promjeniGrupuStudentu = {studentId: studentiPromjeniGrupu[i].id, grupaId: grupaId};
                            PromjeniGrupuStudentu(promjeniGrupuStudentu,predmetId);
                        }
                    }

                    if(studentiNisuUpisani.length != 0) {
                        for(var i = 0; i < studentiNisuUpisani.length; i++) {
                            var dodajStudenta = {ime: studentiNisuUpisani[i].ime, index: studentiNisuUpisani[i].index}
                            DodajStudenta(dodajStudenta,grupaId);
                        }
                    }

                    var poruka = "";

                    for(var i = 0; i < studentiGreska.length; i++) {
                        poruka += "Postoji sudent sa indeksom " + " " + studentiGreska[i].index + "\n";
                    }
                    res.json({message: poruka});
                });
            });
        });    
    });

});

//spirala3
app.get('/v1/predmeti', function(req, res){
    fs.readFile('predmeti.txt', function(err,data) {
        if(err) throw err;
        listaPredmeta = [];
        var predmeti = [];
        var tekst = data.toString().split('\r\n');
        if(tekst[0] != "") {
            for (var i = 0; i < tekst.length; i++) {
                predmeti.push({naziv: tekst[i]});
                listaPredmeta.push(tekst[i]);
            }     
        }
        res.json(predmeti); 
    });
});

app.get('/v1/aktivnosti', function(req, res){
    fs.readFile('aktivnosti.txt', function(err,data) {
        if(err) throw err;
        listaAktivnosti = [];
        var aktivnosti = [];
        var tekst = data.toString().split('\r\n');
        if(tekst[0] != "") {
            for (var i = 0; i < tekst.length; i++) {
                var aktivnost = [...(tekst[i].split(','))] ;
                aktivnosti.push({naziv: aktivnost[0], tip: aktivnost[1], pocetak: parseFloat(aktivnost[2]), kraj: parseFloat(aktivnost[3]), dan: aktivnost[4]});
                listaAktivnosti.push({naziv: aktivnost[0], tip: aktivnost[1], pocetak: parseFloat(aktivnost[2]), kraj: parseFloat(aktivnost[3]), dan: aktivnost[4]});            
            }
        }
        res.json(aktivnosti);
    });
});

app.get('/v1/predmet/:naziv/aktivnost', function(req, res){
    fs.readFile('aktivnosti.txt', function(err,data) {
        if(err) throw err;
        var aktivnosti = [];
        var tekst = data.toString().split('\r\n');
        for (var i = 0; i < tekst.length; i++) {
            var aktivnost = [...tekst[i].split(',')];
            if(aktivnost[0] == req.params.naziv) {
                aktivnosti.push({naziv: aktivnost[0], tip: aktivnost[1], pocetak: parseFloat(aktivnost[2]), kraj: parseFloat(aktivnost[3]), dan: aktivnost[4]});
            }            
        }     
        res.json(aktivnosti);
    });
});

app.post('/v1/predmet',function(req,res){
    let tijelo = req.body;
    let novaLinija = tijelo['naziv'];
    if(listaPredmeta.length == 0 && novaLinija != "") {
        fs.appendFile('predmeti.txt',novaLinija.toUpperCase(),function(err){
            if(err) throw err;
            listaPredmeta.push(novaLinija);
            res.json({message: "Uspješno dodan predmet!"});
        });
    }
    else if(!listaPredmeta.includes(novaLinija.toUpperCase()) && novaLinija != "") {
        fs.appendFile('predmeti.txt',"\r\n"+novaLinija,function(err){
            if(err) throw err;
            listaPredmeta.push(novaLinija);
            res.json({message: "Uspješno dodan predmet!"});
        });
    }
    else {
        res.json({message: "Naziv predmeta postoji!"});
    }
});

app.post('/v1/aktivnost',function(req,res){
    let tijelo = req.body;
    let novaLinija = tijelo['naziv']+","+tijelo['tip']+
        ","+tijelo['pocetak']+","+tijelo['kraj']+","+tijelo['dan'];
    var novaAktivnostJSON = {naziv: tijelo['naziv'], tip: tijelo['tip'], pocetak: parseFloat(tijelo['pocetak']),
                             kraj: parseFloat(tijelo['kraj']), dan: tijelo['dan']};
    if(!(novaAktivnostJSON.pocetak >= 8 && novaAktivnostJSON.pocetak < 20) 
        || !(novaAktivnostJSON.kraj > 8 && novaAktivnostJSON.kraj <= 20) 
        || novaAktivnostJSON.pocetak >= novaAktivnostJSON.kraj
        || novaAktivnostJSON.pocetak % 0.5 != 0 || novaAktivnostJSON.kraj % 0.5 != 0
        || novaAktivnostJSON.naziv == "" || novaAktivnostJSON.pocetak == null || novaAktivnostJSON.kraj == null
        || novaAktivnostJSON.pocetak == "" || novaAktivnostJSON.kraj == ""){
        res.json({message: "Aktivnost nije validna!"});
    }
    else if(listaAktivnosti.length == 0) {
        fs.appendFile('aktivnosti.txt',novaLinija,function(err){
            if(err) throw err;
            listaAktivnosti.push(novaAktivnostJSON);
            res.json({message: "Uspješno dodana aktivnost!"});
        });
    } 
    else{ 
        var brojac = 0;
        for(var i = 0; i < listaAktivnosti.length; i++) {
            if(listaAktivnosti[i].dan == novaAktivnostJSON.dan && 
               ((listaAktivnosti[i].pocetak <= novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj > novaAktivnostJSON.pocetak) ||
               (listaAktivnosti[i].pocetak < novaAktivnostJSON.kraj && listaAktivnosti[i].kraj >= novaAktivnostJSON.kraj) ||
               (listaAktivnosti[i].pocetak > novaAktivnostJSON.pocetak && listaAktivnosti[i].kraj < novaAktivnostJSON.kraj))) 
                    brojac++;
        }
        if(brojac == 0) {
            fs.appendFile('aktivnosti.txt',"\r\n"+novaLinija,function(err){
                if(err) throw err;
                listaAktivnosti.push(novaAktivnostJSON);
                res.json({message: "Uspješno dodana aktivnost!"});
            });
        }
        else
            res.json({message: "Aktivnost nije validna!"}); 
    }
});

app.delete('/v1/predmet/:naziv', function(req, res){
    var predmet = req.params.naziv;
    if(listaPredmeta.includes(predmet)) {
        var index = listaPredmeta.indexOf(predmet);
        listaPredmeta.splice(index,1);
        fs.writeFile('predmeti.txt', "", function(err){
            if(err) throw err;
            for(var i = 0; i < listaPredmeta.length; i++) {
                
                if(i == 0) {
                    fs.appendFile('predmeti.txt',listaPredmeta[i],function(err){
                        if(err) throw err;
                    });    
                }
                else{
                    fs.appendFile('predmeti.txt', "\r\n" + listaPredmeta[i],function(err){
                        if(err) throw err;
                    });
                }
                
            }
            res.json({message:"Uspješno obrisan predmet!"});
        }); 
    }
    else {
        res.json({message:"Greška - predmet nije obrisan!"});
    }
});

app.delete('/v1/aktivnost/:naziv', function(req, res){
    var predmet = {naziv: req.params.naziv};
    var brojac = 0;
    var j = 0;
    predmet = predmet.naziv;
    var aktivnost = JSON.stringify(listaAktivnosti).toString();
    while(aktivnost.includes(predmet)) {
        var index = listaAktivnosti.indexOf(predmet);
        listaAktivnosti.splice(index,1);
        brojac++;
        aktivnost = JSON.stringify(listaAktivnosti).toString();
        fs.writeFile('aktivnosti.txt', "", function(err){
            if(err) throw err;
            for(var i = 0; i < listaAktivnosti.length; i++) {
                var aktivnost = listaAktivnosti[i].naziv + "," + listaAktivnosti[i].tip + "," +  
                                listaAktivnosti[i].pocetak + "," + listaAktivnosti[i].kraj + "," +
                                listaAktivnosti[i].dan;
                if(i == 0) {
                    fs.appendFile('aktivnosti.txt',aktivnost,function(err){
                        if(err) throw err;
                    });
                    j++;    
                }
                else{
                    fs.appendFile('aktivnosti.txt', "\r\n" + aktivnost,function(err){
                        if(err) throw err;
                    });
                    j++;
                }
            }
        }); 
    }
    if(brojac != 0) {
        res.json({message:"Uspješno obrisana aktivnost!"});
    }
    else {
        res.json({message:"Greška - aktivnost nije obrisana!"});
    }
});

app.delete('/v1/all', function(req,res){
    var brojac = 0;
    fs.readFile('aktivnosti.txt',function(err,data){
        if(err) throw err;
        fs.readFile('predmeti.txt',function(err,data){
            if(err) throw err;
            fs.writeFile('aktivnosti.txt',"",function(err){
                if(err) throw err;
                listaAktivnosti = [];
                brojac++;
                fs.writeFile('predmeti.txt',"",function(err){
                    if(err) throw err;
                    listaPredmeta = [];
                    brojac++;
                    if(brojac == 2) {
                        brojac == 0;
                        res.json({message: "Uspješno obrisan sadržaj datoteka!"});
                    }
                    else {
                        res.json({message: "Greška - sadržaj datoteka nije moguće obrisati!"});
                    }
                });
            });
        });
    });
});
app.listen(3000);