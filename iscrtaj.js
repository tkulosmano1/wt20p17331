var Raspored = (function(){
    var pocetakRasporeda; 
    var krajRasporeda;

    function iscrtajRasporedImpl(div,dani,satPocetak,satKraj) {
        pocetakRasporeda = satPocetak;
        krajRasporeda = satKraj;
        this.dani = dani;

        if(satPocetak >= satKraj || satKraj < 0 || satKraj >24 || satPocetak < 0 || satPocetak >24 || !Number.isInteger(satPocetak) || !Number.isInteger(satKraj)) {
            div.innerText = "Greška";
        }
        
        else {
            var tabela = document.createElement("table");
            var trSati = document.createElement("tr");
            var sati = [0,-1,2,-1,4,-1,6,-1,8,-1,10,-1,12,-1,-1,15,-1,17,-1,19,-1,21,-1,23];
            var satiZapis = ["00:00","","02:00","","04:00","","06:00","","08:00","","10:00","","12:00","","","15:00","","17:00","","19:00","","21:00","","23:00"];   
            
            var tdSati1;
            tdSati1 = document.createElement("td");
            tdSati1.setAttribute("class", "td-dani");  
            tdSati1.colSpan = 5;
            trSati.appendChild(tdSati1);

            for(var i = satPocetak; i < 24; i++) {
                
                if(i == sati[i] && satPocetak <= sati[i] && satKraj > sati[i]) {
                    var tdSati = document.createElement("td");
                    tdSati.setAttribute("class", "td-sati");
                    tdSati.textContent = satiZapis[i];
                    tdSati.colSpan = 2;
                    trSati.appendChild(tdSati);
                }

                if(sati[i] == -1){
                    var tdSati = document.createElement("td");
                    tdSati.setAttribute("class", "td-sati");
                    tdSati.colSpan = 2;
                    trSati.appendChild(tdSati);
                }
                
            }
            
            tabela.appendChild(trSati);

            for(var i = 0; i < dani.length; i++) {

                var trKolone = document.createElement("tr");

                for(var j = 0; j < (satKraj-satPocetak)*2+1; j++) {
                    //Ispisuje dane u sedmici
                    if(j < 1) {
                        
                        var tdDani = document.createElement("td");
                        tdDani.setAttribute("class", "td-dani");
                        tdDani.textContent = dani[i];
                        tdDani.colSpan = 5;
                        trKolone.appendChild(tdDani);

                        var tdDani3 = document.createElement("td");
                        tdDani3.setAttribute("class", "td-dani");
                        trKolone.appendChild(tdDani3);

                    }
                    //Crta celiju kojoj je isprekidana linija na desnoj strani
                    if(j % 2 != 0 && j > 0) {
                        var tdSlobodanD = document.createElement("td");
                        tdSlobodanD.setAttribute("class","td-slobodanD");
                        trKolone.appendChild(tdSlobodanD);
                    }
                    //Crta celiju kojoj je isprekidana linija na lijevoj strani
                    else if( j % 2 == 0 && j > 0) {
                        var tdSlobodanL = document.createElement("td");
                        tdSlobodanL.setAttribute("class","td-slobodanL");
                        trKolone.appendChild(tdSlobodanL);
                    }

                }
                tabela.appendChild(trKolone);
            }
            div.appendChild(tabela);
        }
        
    }

    function dodajAktivnostImpl(raspored, naziv, tip, vrijemePocetak, vrijemeKraj, dan) {
        
        if(vrijemePocetak % 0.5 != 0 || vrijemeKraj % 0.5 != 0 || vrijemePocetak >= vrijemeKraj || vrijemeKraj < 0 || vrijemeKraj >24 || vrijemePocetak < 0 || vrijemePocetak >24) {
            alert("GreŠka - neispravan unos početka ili kraja nastave");
            return;
        }

        if(raspored.getElementsByTagName("table").length == 0) {
            alert("Greška - raspored nije kreiran");
            return;
        }

        var tabela = raspored.getElementsByTagName("table")[0];        
        var redDan = this.dani.indexOf(dan) + 1;

        if(!this.dani.includes(dan) || pocetakRasporeda > vrijemePocetak || krajRasporeda < vrijemePocetak || krajRasporeda < vrijemeKraj || pocetakRasporeda > vrijemeKraj) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
            return;
        }

        var tr = tabela.rows[redDan];
        
        var indexPocetka = (vrijemePocetak - pocetakRasporeda)*2 + 2;
        var vrijemeTrajanja = vrijemeKraj - vrijemePocetak;
        var brojZauzetihCelija = vrijemeTrajanja * 2;  

        for(var i = 0; i < brojZauzetihCelija; i++) {
            var td = tr.cells[indexPocetka+i];
            if(td.style.backgroundColor == "lightblue"){
                alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
                return;
            }
        }

        for(var i = 0; i < brojZauzetihCelija-1; i++) {
            var td = tr.cells[indexPocetka+i];
            td.style.backgroundColor = "lightblue";
            td.style.display = "none";
        }

        var td = tr.cells[indexPocetka + brojZauzetihCelija-1];
        
        //Ako su je pocetak i kraj nastave zadan na pola sata   
        if((vrijemePocetak * 10) % 10 != 0 && (vrijemeKraj * 10) % 10 != 0 )
            td.style.borderLeft = "0.5px dashed black";
        //Ako je pocetak nastave jednak pocetku rasporeda 
        if(vrijemePocetak == pocetakRasporeda)
            td.style.borderLeft = "0.5px solid black";

        td.colSpan = brojZauzetihCelija;
        td.style.backgroundColor = "lightblue";   
        td.classList.add("td-zauzeto");
        td.innerText = naziv + "\n" + tip;

    }

    return {
        iscrtajRaspored: iscrtajRasporedImpl,
        dodajAktivnost: dodajAktivnostImpl
    }
}()); 